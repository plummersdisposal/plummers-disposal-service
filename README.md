Plummer’s Disposal prides ourselves on our proven record of customer satisfaction for dumpster rental and portable restrooms. We provide world class service to special events, construction, industrial, and residential clients.


Address: 1160 Electric Ave, Wayland, MI 49348, USA

Phone: 616-261-4344

Website: https://plummersdisposal.com
